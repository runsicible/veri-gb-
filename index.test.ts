import { Account } from './components/Account';
import { StatusCodes } from './components/constants/statusCodes';

describe('suite tests for Bank and Account entities', () => {
    it('check if account doesn`t start with negative balance', () => {
        const account: Account = Account.createAccount(-10, 'Pedro');
        expect(account).toBeNull();
    });
    it('deposits should increment client balance ', () => {
        const account: Account = Account.createAccount(10, 'Pedro');
        const result: StatusCodes = account.bank.deposit(10);
        expect(result).toBe(StatusCodes.successTransaction);
    });
    it('should not deposit negative amount', () => {
        const account: Account = Account.createAccount(100, 'Pedro');
        const result: StatusCodes = account.bank.deposit(-10);
        expect(result).toBe(StatusCodes.negativeDeposits);
    });
    it('client can witdraw money', () => {
        const account: Account = Account.createAccount(100, 'Pedro');
        const result: StatusCodes = account.bank.withDraw(10);
        expect(result).toBe(StatusCodes.successWithDraw);
    });
    it('client can`t witdraw money than it has', () => {
        const account: Account = Account.createAccount(10, 'Pedro');
        const result: StatusCodes = account.bank.withDraw(-20);
        expect(result).toBe(StatusCodes.negativeWithdraw);
    });
    it('client should not withdraw negative amount ', () => {
        const account: Account = Account.createAccount(100, 'Pedro');
        const result: StatusCodes = account.bank.withDraw(-10);
        expect(result).toBe(StatusCodes.negativeWithdraw);
    });
    it('client can transfer to other account', () => {
        const account_1: Account = Account.createAccount(100, 'Pedro');
        const account_2: Account = Account.createAccount(100, 'Juan');
        const result: StatusCodes = account_1.bank.transfer(account_2, 10);
        expect(result).toBe(StatusCodes.successTransfer);
    });
    it('client can`t transfer more money than they have', () => {
        const account_1: Account = Account.createAccount(100, 'Pedro');
        const account_2: Account = Account.createAccount(100, 'Pedro');
        const result: StatusCodes = account_1.bank.transfer(account_2, 200);
        expect(result).toBe(StatusCodes.insuficientAccountToTransfer);
    });
    it('client can`t transfer a negative amount', () => {
        const account_1: Account = Account.createAccount(100, 'Pedro');
        const account_2: Account = Account.createAccount(100, 'Pedro');
        const result: StatusCodes = account_1.bank.transfer(account_2, -200);
        expect(result).toBe(StatusCodes.negativeTransfer);
    });
});
