import { Account } from './components/Account';

const account: Account = Account.createAccount(100, 'Juan');
account.bank.deposit(10);
account.bank.deposit(20);
account.bank.deposit(30);

