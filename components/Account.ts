import  { Bank } from "./Bank";
import { StatusCodes } from "./constants/statusCodes";

export class Account {
    private balance: number;
    private name: string;
    public bank: Bank;
    constructor(balance: number, name: string) {
        this.balance = balance;
        this.name = name;
        this.bank = new Bank(this);
    }
    

    static createAccount(initialBalance: number, name: string): Account {
        if (initialBalance < 0 ) return null;
        return new Account(initialBalance, name);
    }
    
    getName(): string {
        return this.name;
    }

    getBalance(): number {
        return this.balance;
    }

    setBalance(balance: number): void {
         this.balance = balance;
    }
}
