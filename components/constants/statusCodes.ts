export enum StatusCodes {
    negativeDeposits = 'negativeDeposits',
    invalidInitialBalance = 'invalidInitialBalance',
    overdraftAmount = 'overdraftAmount',
    negativeWithdraw = 'negativeWithdraw',
    successTransaction = 'successTransaction',
    successStartedAccount = 'successStartedAccount',
    successTransfer = 'successTransfer',
    negativeTransfer = 'negativeTransfer',
    insuficientAccountToTransfer = 'insuficientAccountToTransfer',
    successWithDraw = 'successWithDraw', 
}
