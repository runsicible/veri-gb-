import { StatusCodes } from '../constants/statusCodes';
import { Account } from '../Account';
export class Logger {
    counter: number = 1;
    log( indexCode: StatusCodes, account?: Account, amount?: number, account_2?: Account) {
        switch (StatusCodes[indexCode]) {
            case (StatusCodes.negativeDeposits):
                console.log(this.counter, ':', 'negative deposit detected');
                this.counter++;
            break;
            case (StatusCodes.successTransaction):
                console.log(this.counter,':', `deposit: ${amount}, balance: ${account.getBalance()}`);
                this.counter++;
            break;    
            case (StatusCodes.negativeWithdraw):
                console.log(this.counter, ':', 'negative withdraw detected');
                this.counter++;
            break;
            case (StatusCodes.overdraftAmount):
                console.log(this.counter, ':', 'overdraft amount detected');
                this.counter++;
            break;
            case (StatusCodes.successWithDraw):
                console.log(this.counter, ':', `withdrawal: ${amount}, balance: ${account.getBalance()}`)
                this.counter++;
            break;
            case (StatusCodes.negativeTransfer):
                console.log(this.counter, ':', 'negative transfer amount detected');
                this.counter++;
            break;
            case (StatusCodes.insuficientAccountToTransfer):
                console.log(this.counter, ':', ' insuficient amount to transfer detected');
                this.counter++;
            break;   
            case (StatusCodes.successTransfer):
                console.log(this.counter, ':', `transfer to : ${account_2.getName()}, ${account_2.getBalance()}, balance: ${account.getBalance()}`)
                this.counter++;
            break;     
            default: 
                console.log(StatusCodes[indexCode], 'number of times ', this.counter);
                this.counter++;
            break;
        }
    }
}