import { Account } from './Account';
import { StatusCodes } from './constants/statusCodes';
import { Logger } from '../components/helpers/logger';

export class Bank {
    private account: Account;
    private logger: Logger;
    constructor(account: Account){
        this.account = account;
        this.logger = new Logger();
    }
     
    transfer(account: Account, amount: number): StatusCodes {
        if (amount <= 0) {
            this.logger.log(StatusCodes.negativeTransfer);
            return StatusCodes.negativeTransfer;
        }
        if (amount > this.account.getBalance()) {
            this.logger.log(StatusCodes.insuficientAccountToTransfer);
            return StatusCodes.insuficientAccountToTransfer;
        }
        account.bank.deposit(amount);
        this.withDraw(amount);
        this.logger.log(StatusCodes.successTransfer, this.account, amount, account);
        return StatusCodes.successTransfer;
    }
    
    deposit(amount: number): StatusCodes {
        if (amount < 0) {
            this.logger.log(StatusCodes.negativeDeposits);
            return StatusCodes.negativeDeposits;
        }
        this.account.setBalance(this.account.getBalance() + amount);
        this.logger.log(StatusCodes.successTransaction, this.account, amount)
        return StatusCodes.successTransaction;
    };

    withDraw(amount: number): StatusCodes {
        if (amount < 0) {
            this.logger.log(StatusCodes.negativeWithdraw)
            return StatusCodes.negativeWithdraw;
        } 
        if (amount > this.account.getBalance()) {
            this.logger.log(StatusCodes.overdraftAmount);
            return StatusCodes.overdraftAmount;
        }
        this.account.setBalance(this.account.getBalance() - amount);
        this.logger.log(StatusCodes.successTransaction, this.account, amount);
        return StatusCodes.successWithDraw;
    }
}
